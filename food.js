
const nutriment = (plat, nutriment) => parseFloat(
  Array.from(plat.querySelectorAll('dt'))  // find all dt's in plat
    .find(el => el.innerHTML.toLowerCase() === nutriment) // keep the one with the nutriment
    .nextElementSibling // get to the dd
    .innerHTML) 

const glucides = plat => nutriment(plat, 'glucides')
const lipides = plat => nutriment(plat, 'lipides')
const calories = plat => nutriment(plat, 'calories')
const proteines = plat => nutriment(plat, 'protéines')

const platEl2js = (plat) => new Food({
  name: plat.querySelector('p').innerHTML,
  weight: parseFloat(plat.attributes['data-weight'].value),
//  caloriesTotal: parseFloat(plat.attributes['data-calories'].value),
//  calories: calories(plat),
  lipides: lipides(plat),
  glucides: glucides(plat),
  proteines: proteines(plat)
})

const plats = Array.from(document.querySelectorAll('.plat')).map(platEl2js)

const plat2tr = p =>
      `<tr>
     <td>${p.name}</td><td>${p.calories().toFixed(0)}</td><td>${p.glucides}</td>
   </tr>
`
const plats2table = (plats) => {
  const table = document.createElement('table')
  table.id="foodlist"
  table.innerHTML = plats.map(plat2tr).join('\n')
  return table
}

const redraw = el => {
  document.querySelector('#foodlist').replaceWith(el)
}

// key t: redraw with table view
document.addEventListener('keyup', e => {
  if (e.key === 't')
    redraw(plats2table(plats))
})

// key l: redraw with list view
document.addEventListener('keyup', e => {
  if (e.key === 'l')
    redraw(plats2ul(plats))
})

const plats2ul = (plats) => {
  const ul = document.createElement('ul')
  ul.id = "foodlist"
  ul.innerHTML = plats.map(plat2li).join('')
  return ul
}

      // <li class="plat" data-weight="100" data-calories="341">
      // <p>Paris brest</p>
      // <dl>
      // <dt>calories</dt>
      // <dd>341</dd>
      // <dt>lipides</dt>
      // <dd>8</dd>
      // <dt>glucides</dt>
      // <dd>33</dd>
      // <dt>protéines</dt>
      // <dd>17</dd>
      // </dl>
      // </li>
const plat2li = (plat) => `
  <li class="plat" data-weight="${plat.weight}" data-calories="${plat.caloriesTotal}">
    <p>${plat.name}</p>
    <dl>
      <dt>calories</dt>
      <dd>${plat.calories().toFixed(0)}</dd>
      <dt>lipides</dt>
      <dd>${plat.lipides}</dd>
      <dt>glucides</dt>
      <dd>${plat.glucides}</dd>
      <dt>protéines</dt>
      <dd>${plat.proteines}</dd>
    </dl>
   </li>
`
function Food(nameOrObject, weight, caloriesTotal, lipides, glucides, proteines){
  if (nameOrObject instanceof Object) {
    Object.assign(this, nameOrObject)
  } else {
    this.name = nameOrObject
    this.weight = weight
    this.caloriesTotal = caloriesTotal
    this.calories = () => this.lipides*9+this.glucides*4+this.proteines*4
    this.lipides = lipides
    this.glucides = glucides
    this.proteines = proteines
  }
}

Food.prototype.calories = function(){ return this.lipides*9+this.glucides*4+this.proteines*4}
